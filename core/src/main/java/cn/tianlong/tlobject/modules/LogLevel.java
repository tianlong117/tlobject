package cn.tianlong.tlobject.modules;

/**
 * 创建日期：${Date}${time}
 * 描述:
 * 作者:tianlong
 */
public enum LogLevel {
    TRACE,DEBUG,INFO,WARN,ERROR
}
