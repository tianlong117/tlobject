package cn.tianlong.tlobject.servletutils.utils;

/**
 * 创建日期：2018/4/5 on 8:47
 * 描述:
 * 作者:tianlong
 */

public interface TLParamString {


    final String URLFILECACHE = "urlFileCache";
    /**    模块名 *      */
    final String URLFILE_P_HOST = "host";
    final String URLFILE_P_FILENAME = "filename";
    final String URLFILE_P_FILEPATH = "filepath";
    final String URLFILE_P_URL = "url";
    final String URLFILE_P_EXPTIME = "exptime";
    final String URLFILE_R_CACHEFILE = "cacheFile";
    final String URLIMAGE_P_HEIGHT = "height";
    final String URLIMAGE_P_WIDTH = "width";
    final String URLIMAGE_P_QUALITY = "quality";
    final String URLIMAGE_P_OUTFILE = "outfile";
}
