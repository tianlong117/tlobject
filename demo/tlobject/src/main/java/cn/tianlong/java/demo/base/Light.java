package cn.tianlong.java.demo.base;


import cn.tianlong.tlobject.base.TLBaseModule;
import cn.tianlong.tlobject.base.TLMsg;
import cn.tianlong.tlobject.base.TLObjectFactory;

public class Light extends DemoCommon {
	private int i =0;
	public Light (String name ){
		super(name);
	}
	public Light (String name,TLObjectFactory moduleFactory)  {
		super(name,moduleFactory);
	}

	@Override
	protected TLBaseModule init() {
		return this;
	}

	@Override
	protected TLMsg checkMsgAction(Object fromWho, TLMsg msg) {
		printAction(msg);
		switch (msg.getAction()){
		case "on" : 
			on( fromWho ,msg) ;
			break ;
		case "off" :
			off(fromWho ,msg); 
			break ;
		default : System.out.println("no action");
		}
		return null;
	}

	private void on (Object fromWho, TLMsg msg) {
		printState("灯打开 " );
		putMsg("house",new TLMsg("light"));
	}
	private void off(Object fromWho, TLMsg msg ) {
		printState("灯关上 " );
		putMsg("house",new TLMsg("dark"));
	}

}
